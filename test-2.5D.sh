#!/bin/bash

n=$1
p=$2
k=$3

if [[ "$4" == 'y' ]]
then
  do_check=1
  cmake_vars="-DDEBUG=On"
else
  do_check=0
  cmake_vars=""
fi

if [[ "$4" == 'nogen' ]]
then
  no_gen=1
else
  no_gen=0
fi

if [[ "$5" == 'clean' ]]
then
  clean_build=1
else
  clean_build=0
fi

if [[ "$OSTYPE" == "darwin"* ]]
then
  build_dir="cmake-build-debug"
  mpi_args="--oversubscribe"
else
  build_dir="build"
  mpi_args=""
fi

TIMEFORMAT="Executed in %4R"

(( p_k=p/k ))

if [[ $no_gen -eq 0 ]]
then
  echo Generating test...
  time python ./utils/test_gen.py "$n" "$p_k" --silent
else
  echo Reusing old tests...
fi

echo Rebuilding...
if [[ clean_build -eq 1 ]]
then
  rm -r ${build_dir:?}/*
fi

cd "$build_dir" && cmake "$cmake_vars" ../ && make && cd ..

echo Running...
time mpiexec -n "$p" $mpi_args "./$build_dir/matmul-2.5d" "$k" 2>/dev/null

if [[ $do_check -eq 1 ]]
then
  echo Running naive...
  time "./$build_dir/matmul-naive" "$n" "$p_k" 2>/dev/null

  echo Checking...
  python ./utils/test_check.py "$n" "$p_k" --silent
fi

unset TIMEFORMAT