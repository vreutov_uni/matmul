#include <mpi.h>
#include <iostream>
#include <fstream>

#include "common/mat.h"
#include "common/mpi.h"

using namespace std;

int w_size, w_rank;

int read_matrix(vec& m, istream& input, int &n, bool transposed = false) {
    input >> n;
    int N = (((n - 1) / w_size) + 1) * w_size;

    resize_mat(m, N);

    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            int idx = transposed ? j * N + i : i * N + j;

            if (i < n && j < n) {
                input >> m[idx];
            } else {
                m[idx] = 0;
            }
        }
    }

    return N;
}

int get_next_in_ring() {
    return (w_rank + 1) % w_size;
}

int get_prev_in_ring() {
    return (w_rank == 0 ? w_size : w_rank) - 1;
}

void calculate_result_part(vec& c, vec& rows, vec& cols, int n, int r_begin, int size) {
    for (int ci = 0; ci < size; ci++) {
        for (int ri = 0; ri < size; ri++) {
            int res = 0;
            for (int i = 0; i < n; i++) {
                res += cols[ci * n + i] * rows[ri * n + i];
            }
            c[ci * n + r_begin + ri] = res;
        }
    }
}

int main(int argc, char** argv) {
    MPIContext ctx;
    w_size = ctx.w_size;
    w_rank = ctx.w_rank;

    int n;
    int a_n{}, b_n{};       // for master
    vec a, b, c;            // for master
    vec rows, cols, c_res;

    if (w_rank == 0) {
        ifstream fileA("input/A.txt");
        n = read_matrix(a, fileA, a_n);
        ctx.log("A:\n%s", to_string(a, a_n).c_str());

        ifstream fileB("input/B.txt");
        read_matrix(b, fileB, b_n, true);
        ctx.log("B:\n%s", to_string(b, b_n, -1, true).c_str());

        if (a_n != b_n) {
            ctx.err("Matrix sizes doesn't match");
        }
        resize_mat(c, n);
    }

    MPI_Bcast(&n, 1, MPI_INT, 0, MPI_COMM_WORLD);
    int chunk_size = n / w_size;

    resize_mat(cols, chunk_size, n);
    resize_mat(rows, chunk_size, n);
    resize_mat(c_res, chunk_size, n);

    MPI_Scatter(
        b.data(), n * chunk_size, MPI_INT,
        cols.data(), n * chunk_size, MPI_INT,
        0, MPI_COMM_WORLD);

    ctx.log("cols:\n%s", to_string(cols, chunk_size, n).c_str());

    MPI_Scatter(
        a.data(), n * chunk_size, MPI_INT,
        rows.data(), n * chunk_size, MPI_INT,
        0, MPI_COMM_WORLD);

    int r_begin = chunk_size * w_rank;
    ctx.log("rows (%d):\n%s", r_begin, to_string(rows, chunk_size, n).c_str());

    ctx.log("next: %d, prev: %d", get_next_in_ring(), get_prev_in_ring());

    ctx.log("running first step");
    calculate_result_part(c_res, rows, cols, n, r_begin, chunk_size);
    ctx.log("step result: %s", to_string(c_res, chunk_size, n, true).c_str());

    for (int step = 1; step < w_size; step++) {
        ctx.log("shifting rows");
        MPI_Sendrecv_replace(
            rows.data(), n * chunk_size, MPI_INT,
            get_prev_in_ring(), step,
            get_next_in_ring(), step,
            MPI_COMM_WORLD, MPI_STATUS_IGNORE);

        r_begin += chunk_size;
        if (r_begin >= n) r_begin -= n;
        ctx.log("rows (%d):", r_begin, to_string(rows, chunk_size, n).c_str());

        ctx.log("running step %d", step + 1);
        calculate_result_part(c_res, rows, cols, n, r_begin, chunk_size);
        ctx.log("step result: %s", to_string(c_res, chunk_size, n, true).c_str());
    }

    ctx.log("gathering result");
    MPI_Gather(
        c_res.data(), c_res.size(), MPI_INT,
        c.data(), c_res.size(), MPI_INT,
        0, MPI_COMM_WORLD);

#ifdef WRITE_OUTPUT
    if (w_rank == 0) {
        ofstream file("output/C.txt");
        file << a_n << endl << to_string(c, n, n, true, a_n) << endl;
    }
#endif
}