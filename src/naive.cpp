#include <iostream>
#include <fstream>
#include <cstdio>
#include <string>
#include <sstream>
#include <cmath>

#include "common/mat.h"

using namespace std;

int N;
int P;

void read_matrix(mat& m, const string& name) {
    resize_mat(m, N);

    for (int pi = 0; pi < P; pi++) {
        for (int pj = 0; pj < P; pj++) {
            stringstream ss; ss << "input/" << name << "_" << pi * P + pj << ".txt";
            ifstream file(ss.str());

            int n = 0; file >> n;

            for (int i = 0; i < n; i++) {
                for (int j = 0; j < n; j++) {
                    file >> m[pi * n + i][pj * n + j];
                }
            }
        }
    }
}

void print_matrix(mat& m, const string& name) {
    stringstream ss; ss << "output/" << name << ".txt";
    ofstream file(ss.str());
    size_t n = m.size();

    file << n << endl;

    for (size_t i = 0; i < n; i++) {
        for (size_t j = 0; j < n; j++) {
            file << m[i][j];
            if (j != n - 1) file << " ";
        }
        file << endl;
    }
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        printf("Usage: %s N P\n", argv[0]);
        printf("    N - matrix size\n");
        printf("    P - processors count\n");
        exit(1);
    }

    N = stoi(argv[1]);
    P = int(sqrt(stoi(argv[2])));

    N = ((N - 1) / P + 1) * P;
    mat A;
    mat B;

#ifdef WRITE_OUTPUT
    read_matrix(A, "A");
    read_matrix(B, "B");
#else
    // dont actually calculate anything
    // i/o takes a lot of time
    resize_mat(A, N);
    resize_mat(B, N);
#endif

    mat C;
    resize_mat(C, N);

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            for (int k = 0; k < N; k++)
                C[i][j] = C[i][j] + A[i][k] * B[k][j];

#ifdef WRITE_OUTPUT
    print_matrix(C, "C_check");
#endif
}

