#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <mpi.h>

#include "common/mpi.h"
#include "common/mat.h"
using namespace std;

int w_size, w_rank;
int k = 2;
int layer_rank, k_rank;
int row_rank, col_rank;

int read_matrix(vec& mat, const string& name) {
    stringstream ss;
    ss << "input/" << name << "_" << w_rank << ".txt";
    ifstream file(ss.str());

    int n; file >> n;
    resize_mat(mat, n);

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            file >> mat[i * n + j];
        }
    }

    return n;
}

void write_matrix(vec& mat, int n, const string& name) {
    stringstream ss;
    ss << "output/" << name << "_" << w_rank << ".txt";
    ofstream file(ss.str());

    file << n << endl;

    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            file << mat[i * n + j];
            if (j != n - 1) file << " ";
        }
        file << endl;
    }
}

int mod(int x, int m) {
    return (x + m) % m;
}

int get_layer_size(const MPIContext& ctx) {
    if (w_size % k != 0) {
        ctx.err("Error: Process count is not divisible by k=%d\n", k);
    }

    double int_part;
    if (modf(sqrt(w_size / k), &int_part) != 0.0) {
        ctx.err("Error: Process count / k must be perfect square (k=%d)\n", k);
    }

    int layer_n = int(int_part);
    if (layer_n % k != 0) {
        ctx.err("Error: Can't divide work for %dx%d mesh for %d layers\n", layer_n, layer_n, k);
    }

    return int(int_part);
}

void add_multiply(vec& a, vec& b, vec& c, int n) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++)
            for (int l = 0; l < n; l++)
                c[i * n + j] += a[i * n + l] * b[l * n + j];
}

int main(int argc, char* argv[]) {
    if (argc > 1) {
        k = stoi(argv[1]);
    }

    MPIContext ctx;
    w_rank = ctx.w_rank;
    w_size = ctx.w_size;

    int layer_n = get_layer_size(ctx);
    int layer_size = layer_n * layer_n;

    // processes with same k
    MPI_Comm layer_comm;
    MPI_Comm_split(MPI_COMM_WORLD, w_rank / layer_size, w_rank, &layer_comm);
    MPI_Comm_rank(layer_comm, &layer_rank);

    // groups along k axis
    MPI_Comm k_comm;
    MPI_Comm_split(MPI_COMM_WORLD, w_rank % layer_size, w_rank, &k_comm);
    MPI_Comm_rank(k_comm, &k_rank);

    // row groups in each layer
    MPI_Comm row_comm;
    MPI_Comm_split(layer_comm, layer_rank / layer_n, layer_rank, &row_comm);
    MPI_Comm_rank(row_comm, &row_rank);

    // columns groups in each layer
    MPI_Comm col_comm;
    MPI_Comm_split(layer_comm, layer_rank % layer_n, layer_rank, &col_comm);
    MPI_Comm_rank(col_comm, &col_rank);

    vec localA;
    vec localB;
    vec localC;
    int n = 0;

    // make aliases
    int p_i = col_rank;
    int p_j = row_rank;
    int p_k = k_rank;
    int size = layer_n;
    int c = k;

    ctx.log("p_i=%d, p_j=%d, p_k=%d, size=%d, c=%d", p_i, p_j, p_k, size, c);

    if (p_k == 0) {
        int n_a = read_matrix(localA, "A");
        int n_b = read_matrix(localB, "B");

        if (n_a != n_b) {
            ctx.err("Matrix sizes doesn't match");
        }

        n = n_a;
    }

    if (k > 1) {
        // broadcast size of block along k-axis
        MPI_Bcast(&n, 1, MPI_INT, 0, k_comm);
    }

    // prepare buffers
    if (k_rank > 0) {
        resize_mat(localA, n);
        resize_mat(localB, n);
    }
    resize_mat(localC, n);

    if (k > 1) {
        // broadcast A_ij and B_ij along k-axis
        MPI_Bcast(localA.data(), localA.size(), MPI_INT, 0, k_comm);
        MPI_Bcast(localB.data(), localB.size(), MPI_INT, 0, k_comm);
    }

    // initial circular shifts
    int col_s = mod(p_j - p_i + (p_k * size / c), size);
    int col_r = mod(p_j + p_i - (p_k * size / c), size);
    int row_s = mod(p_i - p_j + (p_k * size / c), size);
    int row_r = mod(p_i + p_j - (p_k * size / c), size);

    stringstream ss;
    ss << ":r" << col_rank << ":c" << row_rank;
    ctx.log_prefix += ss.str();

    for (int i = 0; i < size / c; i++) {
        if (col_s != row_rank) {
            ctx.log("@ %d: shift A: %d -> %d", i, row_rank, col_s);

            MPI_Sendrecv_replace(
                localA.data(), localA.size(), MPI_INT, col_s, i,
                col_r, i, row_comm, MPI_STATUS_IGNORE);
        } else {
            ctx.log("@ %d: shift A: not needed", i);
        }

        if (row_s != col_rank) {
            ctx.log("@ %d: shift B: %d -> %d", i, col_rank, row_s);

            MPI_Sendrecv_replace(
                localB.data(), localB.size(), MPI_INT, row_s, i,
                row_r, i, col_comm, MPI_STATUS_IGNORE);
        } else {
            ctx.log("@ %d: shift B: not needed", i);
        }

        add_multiply(localA, localB, localC, n);

        col_s = mod(p_j + 1, size);
        col_r = mod(p_j - 1, size);
        row_s = mod(p_i + 1, size);
        row_r = mod(p_i - 1, size);
    }

    vec globalC;
    if (k_rank == 0) {
        resize_mat(globalC, n);
    }

    if (k > 1) {
        MPI_Reduce(
            localC.data(), globalC.data(), localC.size(),
            MPI_INT, MPI_SUM, 0, k_comm);
    } else {
        globalC.swap(localC);
    }

#ifdef WRITE_OUTPUT
    if (k_rank == 0) {
        write_matrix(globalC, n, "C");
    }
#endif
}