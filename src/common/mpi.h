#pragma once

#include <string>
#include <sstream>
#include <iomanip>
#include <cstdlib>
#include <cmath>
#include <mpi.h>

class MPIContext {
public:
    explicit MPIContext() {
        MPI_Init(nullptr, nullptr);
        MPI_Comm_size(MPI_COMM_WORLD, &w_size);
        MPI_Comm_rank(MPI_COMM_WORLD, &w_rank);

        std::stringstream filename;
        filename << "logs/p" << w_rank << ".txt";

        logfile.open(filename.str(), std::ios::out | std::ios::trunc);

        std::stringstream ss;
        int w = int(floor(log10(w_size))) + 1;
        ss << std::setw(w) << w_rank;
        log_prefix = ss.str();
    }

    virtual ~MPIContext() {
        logfile.close();
        exit(0);
    }

    static void exit(int code) {
        MPI_Finalize();
        ::exit(code);
    }

    void err(const char *format, ...) const
    {
        if (w_rank == 0) {
            va_list args;
            va_start(args, format);
            vfprintf(stdout, format, args);
            va_end(args);
        }

        // exit normally to not produce a ton of errors
        exit(0);
    }

    void log(const char *format, ...) const
    {
        va_list args;
        va_start(args, format);
        _log(format, args);
        va_end(args);
    }

    void _log(const char* format, va_list args) const {
#ifdef WRITE_OUTPUT
        std::stringstream log_format;
        log_format << "[" << log_prefix << "] " << format << std::endl;

        vfprintf(stderr, log_format.str().c_str(), args);
#endif
    }

public:
    int w_rank{};
    int w_size{};
    std::string log_prefix;

private:
    std::ofstream logfile;
};

