#pragma once
#include <vector>
#include <string>
#include <sstream>

typedef std::vector<int> vec;
typedef std::vector<vec> mat;

void resize_mat(vec& mat, int n, int m = -1) {
    if (m == -1) m = n;

    mat.resize(n * m, 0);
}

void resize_mat(mat& mat, int n, int m = -1) {
    if (m == -1) m = n;

    mat.resize(n, vec(m, 0));
}

std::string to_string(const vec& m, int rc, int cc = -1, bool transposed = false, int n_crop = -1) {
    if (cc == -1) {
        cc = rc;
    }

    if (transposed) {
        std::swap(rc, cc);
    }

    std::stringstream ss;
    for (int i = 0; i < rc; i++) {
        for (int j = 0; j < cc; j++) {
            if (n_crop > 0 && (i >= n_crop || j >= n_crop))
                continue;

            int idx;
            if (transposed) {
                idx = j * rc + i;
            } else {
                idx = i * cc + j;
            }
            ss << m[idx];
            if (j != cc - 1 && j != n_crop - 1) ss << " ";
        }
        if (i != rc - 1) ss << std::endl;
    }
    return ss.str();
}

