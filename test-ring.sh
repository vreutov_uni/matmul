#!/bin/bash

n=$1
p=$2

if [[ "$OSTYPE" == "darwin"* ]]
then
  build_dir="cmake-build-debug"
  mpi_args="--oversubscribe"
else
  build_dir="build"
  mpi_args=""
fi

TIMEFORMAT="Executed in %4R"

echo Generating test...
time python ./utils/test_gen.py "$n" "$p" --silent

echo Rebuilding...
cd "$build_dir" && make && cd ..

echo Running...
time mpiexec -n "$p" $mpi_args "./$build_dir/matmul-ring" 2>/dev/null

if [[ "$3" == 'y' ]]
then
  echo Running naive...
  "./$build_dir/matmul-naive" "$n" "$p"
  time "./$build_dir/matmul-naive" "$n" "$p" 2>/dev/null

  echo Checking...
  python ./utils/test_check.py "$n" "$p" --silent --single 2>/dev/null
fi

unset TIMEFORMAT
