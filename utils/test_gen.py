import shutil
import os
import random

from common import get_base_arg_parser, parse_args, create_logger

parser = get_base_arg_parser('Generate separated matrix input for p processors')
parser.add_argument('--Be', dest='Be', help='Generate B as identity matrix', action='store_true')
args = parser.parse_args()
n, N, sqrt_p, block_size = parse_args(args)

logger = create_logger("test_gen", args.silent)
logger.info("n = {}, N = {}, sqrt_p = {}, block_size = {}".format(n, N, sqrt_p, block_size))

if os.path.exists("input"):
    shutil.rmtree("input")
if os.path.exists("output"):
    shutil.rmtree("output")
os.mkdir("input")
os.mkdir("output")

for name in ['A', 'B']:
    if name == 'B' and args.Be:
        matrix = [
            [
                (1 if i == j else 0) if i < n and j < n else 0
                for j in range(N)
            ]
            for i in range(N)
        ]
    else:
        matrix = [
            [
                random.randint(0, 100) if i < n and j < n else 0
                for j in range(N)
            ]
            for i in range(N)
        ]

    pn = 0

    logger.info("Full matrix {}:".format(name))
    for i in range(N):
        logger.info(' '.join(map(str, matrix[i])))

    with open("input/{}.txt".format(name, pn), 'w') as file:
        file.write(str(N) + '\n')

        for i in range(N):
            file.write(" ".join(map(str, matrix[i])))
            file.write('\n')

    for i_offset in range(0, N, block_size):
        for j_offset in range(0, N, block_size):
            with open("input/{}_{}.txt".format(name, pn), 'w') as file:
                file.write(str(block_size) + '\n')

                for i in range(block_size):
                    file.write(" ".join(
                        str(matrix[i_offset + i][j_offset + j])
                        for j in range(block_size)
                    ))
                    file.write('\n')
            pn += 1

print("Generation done")
exit(0)
