import argparse
import logging
import math
import os
import sys


def create_logger(name, silent):
    """Usage: logger = create_logger(__name__)"""
    logger = logging.getLogger(name)
    logger.setLevel(logging.DEBUG)

    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    sh = logging.StreamHandler(sys.stderr)
    sh.setFormatter(formatter)
    logger.addHandler(sh)

    if not os.path.exists("utils/logs"):
        os.mkdir("utils/logs")
    fh = logging.FileHandler("utils/logs/{}.log".format(name))
    fh.setFormatter(formatter)
    logger.addHandler(fh)

    if silent:
        logger.disabled = True

    return logger


def get_base_arg_parser(info):
    parser = argparse.ArgumentParser(info)
    parser.add_argument('n', help='Matrix size', type=int)
    parser.add_argument('p', help='Processor count', type=int)
    parser.add_argument('--silent', dest='silent', help='Dont do logs', action='store_true')
    return parser


def parse_args(args):
    n = args.n
    p = args.p

    sqrt_p = math.sqrt(p)

    if sqrt_p != int(sqrt_p):
        print("Error: p must be perfect square")
        exit(1)

    sqrt_p = int(sqrt_p)
    # offset = n % sqrt_p
    ceil_n = ((n - 1) // sqrt_p + 1) * sqrt_p
    block_size = ceil_n // sqrt_p

    return n, ceil_n, sqrt_p, block_size
