from common import get_base_arg_parser, parse_args, create_logger

logger = None


def read_scattered(path_template, n, block_size):
    mat = [
        [0 for _ in range(n)]
        for _ in range(n)
    ]

    pn = 0
    for i_offset in range(0, n, block_size):
        for j_offset in range(0, n, block_size):
            with open(path_template.format(pn), 'r') as file:
                lines = file.readlines()

            for i in range(block_size):
                values = list(map(int, lines[1 + i].split(' ')))
                for j in range(block_size):
                    mat[i_offset + i][j_offset + j] = values[j]
            pn += 1

    logger.info("Full matrix {}:".format(path_template.format('*')))
    for i in range(n):
        logger.info(' '.join(map("{:3}".format, mat[i])))

    return mat


def read_single(path, n):
    mat = [
        [0 for _ in range(n)]
        for _ in range(n)
    ]

    with open(path, 'r') as file:
        lines = file.readlines()

    for i in range(n):
        values = list(map(int, lines[1 + i].split(' ')))
        for j in range(n):
            mat[i][j] = values[j]

    return mat


def main():
    global logger

    parser = get_base_arg_parser('Generate separated matrix input for p processors')
    parser.add_argument('--single', dest='single', help='Read C from single output file', action='store_true')
    args = parser.parse_args()
    logger = create_logger("test_check", args.silent)
    n, ceil_n, sqrt_p, block_size = parse_args(args)

    if args.single:
        c = read_single('output/C.txt', ceil_n)
    else:
        c = read_scattered('output/C_{}.txt', ceil_n, block_size)

    c_check = read_single('output/C_check.txt', ceil_n)

    for i in range(ceil_n):
        for j in range(ceil_n):
            if c[i][j] != c_check[i][j]:
                logger.info("Fail at i={}, j={}: expected {}, got {}".format(
                    i, j, c_check[i][j], c[i][j]
                ))
                print("Failed!")
                exit(1)
    else:
        print("Passed!")
        exit(0)


if __name__ == '__main__':
    main()
