#!/bin/bash

n=$1
p=$2
k=$3

if [[ "$OSTYPE" == "darwin"* ]]
then
  build_dir="cmake-build-debug"
  mpi_args="--oversubscribe"
else
  build_dir="build"
  mpi_args=""
fi

(( p_k=p/k ))

echo "== Test on equal number of processes"
echo "Hint: configuration p=64, k=4 works on both"
echo Rebuilding...
cd "$build_dir" && make && cd ..

TIMEFORMAT="Executed in %3lR"

echo "===== Ring"
echo Generating test...
time python ./utils/test_gen.py "$n" "$p" --silent

echo Running ring...
time mpiexec -n "$p" $mpi_args "./$build_dir/matmul-ring" 2>/dev/null

echo "===== 2.D"
echo Generating test...
time python ./utils/test_gen.py "$n" "$p_k" --silent

echo Running 2.5D...
time mpiexec -n "$p" $mpi_args "./$build_dir/matmul-2.5d" "$k" 2>/dev/null

unset TIMEFORMAT
